# Dispatch Example App #

This project contains a dummy webservice app, and a client to consume that web service.
The client uses [Dispatch](http://dispatch.databinder.net/) to get the root resource, 
and recursively follow links in the REST responses.

The dummy webservice uses data from the [Open Music Project](http://musicmoz.org/about.html)'s
'releases' database.

I have also set up a [Heroku app](http://dispatch-example.kaeding.name/) running this 
server. 

## Build & Run ##

```sh
$ cd dispatch-example
$ ./sbt start-script
```

This will build the app (both the client and server are in there).

### To run the server ###  

```sh
$ target/start name.kaeding.dispatchexample.JettyLauncher
```

### To run the client ###

This command will connect the client to a server running on http://localhost:8080,
so be sure to start the server first.

```sh
$ target/start name.kaeding.dispatchexample.GetTitles
```

To connect to another server (like the one running in Heroku), run it thusly:

```sh
$ target/start name.kaeding.dispatchexample.GetTitles http://dispatch-example.kaeding.name/
```

The client will make many, many requests to the server, following links in one response
to make the next response.  The results (musical releases, in this case), are then
all collected, and printed to the console.

## Why is this interesting? ##

I never said it was interesting ;)

It is an illustration of a simple usage of the Dispatch client library to crawl through
a REST API, without all the other stuff going on that would be in a real application.

## What is this data in the API anyway? ##

It is just the first freely-available, hierarchical data I could find.

### Help build the largest human-contributed music project online ###
- [Submit Information](http://musicmoz.org/cgi-bin/add.cgi?where=Releases)
- [Open Music Project](http://musicmoz.org/about.html)
- [Become an Editor](http://musicmoz.org/cgi-bin/apply.cgi?where=Releases)