import com.typesafe.startscript.StartScriptPlugin

organization := "name.kaeding"

name := "Dispatch Example App"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.9.1"

seq(webSettings :_*)

seq(StartScriptPlugin.startScriptForClassesSettings: _*)

classpathTypes ~= (_ + "orbit")

libraryDependencies ++= Seq(
  "net.databinder.dispatch" %% "dispatch-core" % "0.9.4",
  "net.liftweb" %% "lift-json" % "2.4",
  "org.scalatra" % "scalatra" % "2.1.1",
  "org.scalatra" % "scalatra-scalate" % "2.1.1",
  "org.scalatra" % "scalatra-specs2" % "2.1.1" % "test",
  "org.eclipse.jetty" % "jetty-webapp" % "8.0.3.v20111011" % "container, compile",
  "org.eclipse.jetty" % "jetty-server" % "8.0.3.v20111011" % "container, compile",
  "ch.qos.logback" % "logback-classic" % "1.0.6" % "runtime",
  "javax.servlet" % "servlet-api" % "2.5" % "provided",
  "org.scalaz" % "scalaz-core_2.9.2" % "7.0.0-M3"
)
