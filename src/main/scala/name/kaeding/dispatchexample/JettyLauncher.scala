package name.kaeding.dispatchexample

import org.slf4j.LoggerFactory
import xml._
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.servlet.{ DefaultServlet, ServletContextHandler, ServletHolder }

object JettyLauncher {
  val logger = LoggerFactory.getLogger(getClass)
  def main(args: Array[String]) {
    val port = if (System.getenv("PORT") != null) System.getenv("PORT").toInt else 8080

    val server = new Server(port)
    val context = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS)

    context.addServlet(new ServletHolder(new MusicMoz()), "/*");
    context.setResourceBase("src/main/webapp")
    context.setAttribute("musicMoz", getMusicMoz)

    server.start
    server.join
  }

  def getMusicMoz = {
    logger.info("loaded musicmoz db")
    XML.load(getClass.getResourceAsStream("/musicmoz.releases.xml"))
  }
}