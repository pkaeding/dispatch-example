package name.kaeding

import scalaz._, Scalaz._
import dispatch._

package object dispatchexample {
  implicit def PromiseMonad: Monad[Promise] = new Monad[Promise] {
    def bind[A, B](pa: Promise[A])(fa: A => Promise[B]) = pa.flatMap(fa)
    def point[A](a: => A) = Promise(a)
  }
}