package name.kaeding
package dispatchexample

import org.scalatra._
import scalate.ScalateSupport
import xml._
import net.liftweb.json._
import net.liftweb.json.Serialization.write
import java.net.URLEncoder

class MusicMoz extends ScalatraServlet {
  implicit val formats = Serialization.formats(NoTypeHints)
  def note(category: String): Map[String, String] = 
    Map("notice" -> "This application uses data derived from the Open Music Project.  Help build the largest human-contributed music project online",
	 	  "submitInformation" -> "http://musicmoz.org/cgi-bin/add.cgi?where=%s".format(category),
		  "openMusicProject" -> "http://musicmoz.org/about.html",
		  "becomeAnEditor" -> "http://musicmoz.org/cgi-bin/apply.cgi?where=%s".format(category))
  get("/") {
    serveCategory("Releases")
  }
  
  get("/*") {
    val categoryName = params("splat")
    serveCategory(categoryName)
  }
  
  def serveCategory(categoryName: String) = {
    val category = getServletContext.getAttribute("musicMoz").asInstanceOf[Elem] \ "category" filter (c => (c \ "@name").text ==  categoryName)
    val ret = category.headOption.map(buildResp).getOrElse(pass)
    contentType = "application/json"
    write(ret)
  }
  
  def buildResp(category: Node) = {
    val name = category \ "@name" text
    val subcategories = for {
      sc <- category \ "subcat"
      scn <- sc \ "@name"
    } yield scn.text
    val titles = for {
      item <- category \ "item"
      title <- item \ "title"
    } yield title.text
    Category(name, subcategories.map(c => "/%s/%s".format(name, URLEncoder.encode(c, "UTF-8"))).toList, titles.toList, note(name))
  }
  
}

sealed case class Category(
    name: String, 
    subCategories: List[String], 
    titles: List[String], 
    notes: Map[String, String])
