package name.kaeding.dispatchexample

import scalaz._, Scalaz._
import dispatch._
import com.ning.http.client._
import net.liftweb.json._
import net.liftweb.json.Serialization.read

object GetTitles extends App {
  implicit val formats = Serialization.formats(NoTypeHints)
  val baseurl = args.headOption.getOrElse("http://localhost:8080")
  val gov = new Governor
  val handler: (Response => Category) = (resp: Response) => 
    read[Category](resp.getResponseBody)
    
  // this should limit the number of threads to 16:
  val h = Http.threads(16)
  
  /**
   * gets all titles from the category at the given url, and recursively follows all
   * subcategories
   */
  def getTitles(categoryUrl: String): Promise[List[String]] = {
    for {
      category <- getCategory(categoryUrl).option
      subcategories <- category.map(_.subCategories.map(sc => getTitles(baseurl + sc))).getOrElse(Nil).sequence.map(_.join)
    } yield category.map(_.titles ++ subcategories).getOrElse(Nil)
  }

  def getCategory(categoryUrl: String): Promise[Category] = {
    gov.requestClearance
    // note that I am not building up the URLs in the 'right' way; I am just 
    // passing in the whole url as a string.
    h(url(categoryUrl) OK handler)
  }
    
  val allTitles = getTitles(baseurl).apply
  // output all the titles to the screen
  allTitles foreach println
}

// prevent it from completely overwhelming the server, and ensure it 
// takes long enough to see what is going on while it runs 
class Governor {
  private[this] var lastReq: Long = 0
  val minTimeBetweenRequests = 10 // in ms
  
  def requestClearance = this.synchronized {
    import java.util.Date
    val now = new Date().getTime
    val timeSince = now - lastReq
    if (timeSince < minTimeBetweenRequests) {
      Thread.sleep(minTimeBetweenRequests - timeSince)
    }
    lastReq = new Date().getTime
  }
}